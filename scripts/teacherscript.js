//Generates scores from localstorage
const populateScoresTable = () => {
    const table = document.querySelector('tbody')

    Object.values(localStorage).forEach(studentEntry => {
        const row = document.createElement('tr')
        const student = JSON.parse(studentEntry)
        console.log(student)
        for (const [key, value] of Object.entries(student)) {
            const tableData = document.createElement('td')
            if (key === "hasPassed") {
                tableData.classList.add(value ? 'passed' : 'failed')
                tableData.textContent =  value ? "Passed" : "Failed"
            } else {
                tableData.textContent = value
            }
            row.appendChild(tableData)
        }
        table.appendChild(row)
    })
}
//Clears all data from local storage
const clearQuiz = () => {
    const data = document.querySelectorAll('td')
    for (const element of data) {
        if (element.parentNode === null) return
        element.parentNode.remove()
    }
    localStorage.clear();
}

populateScoresTable()//Generate content for scores

