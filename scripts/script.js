const xhr = new XMLHttpRequest()
xhr.open("GET", "../database/questions.json", true)//obtain the questions from the database directory

const questionParts = []//Declare a global variable array that will be used to store the questions and answers

xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
        const res = xhr.responseText
        const json = JSON.parse(res)
        const questions = json.questions
        populateQuiz(questions)
        configureDragAndDrop()
    }
}

xhr.send()
//This function is reponsible for dynamically generating the content of the web page
const populateQuiz = (questions) => {
    const { partOne, partTwo, partThree, partFour } = questions

    questionParts.push(partOne, partTwo, partThree, partFour)//populate the array

    const part1Form = document.createElement("form")
    const part2Form = document.createElement("form")
    const part3Form = document.createElement("form")
    const part4Form = document.createElement("form")

    part1Form.className = "part1Form"
    part2Form.className = "part2Form"
    part3Form.className = "part3Form"
    part4Form.className = "part4Form"


    const sections = document.querySelectorAll(".questions")//selects the questions section
    sections.forEach((section, sectionIndex) => {
        if (sectionIndex === 0) {
            const questions = getRandomQuestions(partOne)//Get 5 random questions for part 1
            questions.forEach((question) => {
                const qElement = createPartOneQuestions(question.question, question.index)//place each question in the document tree
                part1Form.appendChild(qElement)
            })
            section.appendChild(part1Form)
        }

        if (sectionIndex === 1) {
            const questions = getRandomQuestions(partTwo)//Get 5 random questions for part 2
            questions.forEach((question) => {
                const qElement = createPartTwoQuestions(question.question, question.choices, question.index)//place each question in the document tree
                part2Form.appendChild(qElement)
            })
            section.appendChild(part2Form)
        }

        if (sectionIndex === 2) {
            const questions = getRandomQuestions(partThree)//Get 5 random questions for part 3
            questions.forEach((question) => {
                const qElement = createPartThreeQuestions(question.question, question.index, question.answer)//place each question and answer in the document tree
                part3Form.appendChild(qElement)
            })
            const choicesContainer = document.querySelector('.choicesContainer')

            for (let i = 0; i < choicesContainer.childNodes.length; i++) {
                choicesContainer.appendChild(choicesContainer.childNodes.item(Math.floor(Math.random() * i)))//Randomize the choices for drag and drop
            }

            section.appendChild(part3Form)
        }

        if (sectionIndex === 3) {
            const questions = getRandomQuestions(partFour)//Get 5 random questions for part 4
            questions.forEach((question) => {
                const qElement = createPartFourQuestions(question.question, question.index)//place each question in the document tree
                part4Form.appendChild(qElement)
            })
            section.appendChild(part4Form)
        }
    })
}
//This function collects user input and invoke necessary methods to validate the result
const submitQuiz = () => {
    const nameInput = document.getElementById('name')
    if (nameInput.value === "" || nameInput.value.length === 0) {
        return alert('Name field should not be empty!')
    }

    const resultsSection = document.getElementById('results')
    resultsSection.classList.remove('hidden')

    const tableRows = document.querySelectorAll('tr')

    tableRows.forEach(row => {
        row.lastChild.remove()
    })

    const partOneScore = checkPartOne()//Validates User Input for part 1
    const partTwoScore = checkPartTwo()//Validates User Input for part 2
    const partThreeScore = checkPartThree()//Validates User Input for part 3
    const partFourScore = checkPartFour()//Validates User Input for part 4

    const total = parseInt(partOneScore + partTwoScore + partThreeScore + partFourScore)//Gets the sum of the scores for each part

    const scores = [
        partOneScore,
        partTwoScore,
        partThreeScore,
        partFourScore,
        total
    ]
    //Displays user score for each part
    tableRows.forEach((row, index) => {
        const tdElement = document.createElement('td')
        tdElement.textContent = scores[index]
        row.appendChild(tdElement)
    })
    //Checks whether student passed or not (60% in order to pass)
    const studentSubmission = {
        name: nameInput.value,
        partOne: partOneScore,
        partTwo: partTwoScore,
        partThree: partThreeScore,
        partFour: partFourScore,
        total,
        hasPassed: total / 20 * 100 > 60
    }

    // const localStorage = Window.localStorage
    localStorage.setItem(nameInput.value, JSON.stringify(studentSubmission))
    document.getElementById("results").scrollIntoView({behavior: 'smooth'});
    document.getElementById("submit_button").disabled = true
}
//Gets 5 random questions from a pool of questions for each part
const getRandomQuestions = (questions) => {
    const randomIndices = new Set()
    while (randomIndices.size < 5) {
        const number = Math.floor(Math.random() * questions.length)
        randomIndices.add(number)
    }
    const questionsArray = questions.map((question, index) => {
        if (randomIndices.has(index)) return question
    })

    return questionsArray.filter(question => question !== undefined)
}
//Generate part 1 questions
const createPartOneQuestions = (question, index) => {
    const questionDiv = document.createElement("div")
    questionDiv.id = index
    questionDiv.className = "question"

    const questionText = document.createElement("span")
    questionText.textContent = question

    const trueRadio = document.createElement("input")
    trueRadio.classList.add(index, 'true')
    trueRadio.setAttribute("type", "radio")
    trueRadio.setAttribute("name", `radio${index}`)
    trueRadio.setAttribute("id", index)

    const falseRadio = document.createElement("input")
    falseRadio.classList.add(index, 'false')
    falseRadio.setAttribute("type", "radio")
    falseRadio.setAttribute("name", `radio${index}`)
    falseRadio.setAttribute("id", index)

    const trueLabel = document.createElement("label")
    trueLabel.setAttribute("for", `radio${index}`)
    trueLabel.textContent = 'True'

    const falseLabel = document.createElement("label")
    falseLabel.setAttribute("for", `radio${index}`)
    falseLabel.textContent = "False"

    const answersDiv = document.createElement("div")
    answersDiv.className="answer"
    answersDiv.appendChild(trueRadio)
    answersDiv.appendChild(trueLabel)
    answersDiv.appendChild(falseRadio)
    answersDiv.appendChild(falseLabel)
    questionDiv.appendChild(document.createElement("br"))
    questionDiv.appendChild(questionText)
    questionDiv.appendChild(document.createElement("br"))
    questionDiv.appendChild(answersDiv)

    return questionDiv
}

//Generate part two questions
const createPartTwoQuestions = (question, choices, index) => {
    const questionDiv = document.createElement("div")
    questionDiv.id = `question${index}`
    questionDiv.className = "question"

    const questionText = document.createElement("span")
    questionText.textContent = question

    const optionsDiv = document.createElement("div")
    optionsDiv.className = "answers"


    for(let i = 0; i < Object.values(choices).length; i++){
        const optionRadio = document.createElement("input")
        optionRadio.setAttribute("type", "radio")
        optionRadio.setAttribute("name", `options${index}`)
        optionRadio.setAttribute("value", Object.keys(choices)[i])
        optionRadio.setAttribute("id", index)

        const optionLabel = document.createElement("label")
        optionLabel.setAttribute("for", `option${i}`)
        optionLabel.textContent = String(Object.values(choices)[i])

        optionsDiv.appendChild(document.createElement("br"))
        optionsDiv.appendChild(optionRadio)
        optionsDiv.appendChild(optionLabel)

    }
    questionDiv.appendChild(document.createElement("br"))
    questionDiv.appendChild(questionText)
    questionDiv.appendChild(optionsDiv)

    return questionDiv
}
//Generate part 3 questions
const createPartThreeQuestions = (question, index, answer) => {
    const questionDiv = document.createElement("div")
    questionDiv.id = `pt3_question${index}`
    questionDiv.className = "question"

    const choicesContainer = document.getElementsByClassName("choicesContainer")[0]
    const choice = document.createElement("h5")
    choice.draggable = true
    choice.className = "mt_choices"
    choice.innerHTML = answer
    choicesContainer.appendChild(choice)

    const questionText = document.createElement("span")
    questionText.textContent = question
    questionDiv.appendChild(questionText)
    questionDiv.appendChild(document.createElement("br"))

    const dropZone = document.createElement('div')
    dropZone.id = index
    dropZone.className = 'dropZone'
    questionDiv.appendChild(dropZone)

    return questionDiv
}
//Generate Part 4 questions
const createPartFourQuestions = (question, index) => {
    const questionDiv = document.createElement("div")
    questionDiv.id = `pt4_question`
    questionDiv.className = "question"

    const questionText = document.createElement("span")
    questionText.textContent = question

    const textInput = document.createElement("input")
    textInput.setAttribute("type", "text")
    textInput.setAttribute("name", `text${index}`)
    textInput.setAttribute("id", index)
    textInput.setAttribute("style", "text-transform: uppercase")

    const answersDiv = document.createElement("div")
    answersDiv.className = 'answers'
    answersDiv.appendChild(textInput)

    questionDiv.appendChild(answersDiv)
    questionDiv.appendChild(questionText)
    questionDiv.appendChild(document.createElement("br"))
    questionDiv.appendChild(document.createElement("br"))

    return questionDiv
}
//Validates Part 1
const checkPartOne = () => {
    let score = 0
    const inputs = document.querySelectorAll('.part1Form div > input')
    const checkedInputs = {}

    for (const input of inputs.entries()) {
        if (input[1].checked) checkedInputs[input[1].classList.item(0)] = input[1].classList.item(1)
    }

    const questionIndices = Object.keys(checkedInputs)

    questionIndices.forEach(index => {
        const databaseAnswer = questionParts[0].filter(q => q.index === parseInt(index))[0].answer
        const userAnswer = checkedInputs[index]
        console.log(`${databaseAnswer}`, userAnswer)
        if (`${databaseAnswer}` === userAnswer) score++
    })

    return score
}
//Validates Part 2
const checkPartTwo = () => {
    let score = 0
    const inputs = document.querySelectorAll('.part2Form div > input')
    const selectedChoices = {}

    for (const choice of inputs.entries()) {
        if (choice[1].checked) {
            selectedChoices[parseInt(choice[1].id)] = choice[1].value
        }
    }

    const questionIndices = Object.keys(selectedChoices)

    questionIndices.forEach(index => {
        const databaseAnswer = questionParts[1].filter(q => q.index === parseInt(index))[0].answer
        const userAnswer = selectedChoices[index]
        if (databaseAnswer === userAnswer) score++
    })

    return score
}
//Validates Part 3
const checkPartThree = () => {
    let score = 0
    const inputs = document.querySelectorAll('.part3Form div > div')

    const matchedAnswers = {}

    for (const answer of inputs.entries()) {
        if (answer[1].lastChild === null) continue
        matchedAnswers[answer[1].id] = answer[1].lastChild.textContent
    }

    const questionIndices = Object.keys(matchedAnswers)

    questionIndices.forEach(index => {
        const databaseAnswer = questionParts[2].filter(q => q.index === parseInt(index))[0].answer
        const userAnswer = matchedAnswers[index]
        console.log(databaseAnswer, userAnswer)
        if (databaseAnswer === userAnswer) score++
    })

    return score
}
//Validates Part 4
const checkPartFour = () => {
    let score = 0
    const inputs = document.querySelectorAll('.part4Form div div > input')

    const answers = {}

    for (const answer of inputs.entries()) {
        answers[answer[1].id] = answer[1].value.toUpperCase()
    }

    const questionIndices = Object.keys(answers)

    questionIndices.forEach(index => {
        const databaseAnswer = questionParts[3].filter(q => q.index === parseInt(index))[0].answer
        const userAnswer = answers[index]
        if (databaseAnswer === userAnswer) score++
    })

    return score
}
//Allow choices for part 3 to be draggable
function configureDragAndDrop(){
    const draggableChoices = document.querySelectorAll(".mt_choices")
    console.log(draggableChoices)
    let dropZones = Array.from(document.querySelectorAll(".dropZone"))
    dropZones.push(document.getElementsByClassName("choicesContainer")[0])
    console.log(dropZones)

// the element currently being dragged
    let currentLyDragging = null;

    draggableChoices.forEach(draggableChoice =>{
        draggableChoice.addEventListener("dragstart", function (){
            currentLyDragging = draggableChoice
        })

        draggableChoice.addEventListener("dragend", function (){
            currentLyDragging = null
        })
    })

    dropZones.forEach(dropZone =>{
        dropZone.addEventListener("dragover", listener =>{
            listener.preventDefault()
            if(dropZone.childElementCount === 0 || dropZone.className === "choicesContainer"){
                dropZone.appendChild(currentLyDragging)
            }
        })
    })
}